import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class TennisGUI extends JFrame {
private JPanel contentPane;
	
	public TennisGUI() {
		setResizable(false);
		setTitle("Tennis Championship League");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(750, 750);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel title = new JLabel("<html><h1><center><strong><i>Tennis Championship League - 2023</i></strong></center></h1><hr></html>");
		title.setBounds(150, 20, 45, 16);
		title.resize(500, 150);
		contentPane.add(title);
		
		JButton league1 = new JButton("Tennis Championship League 01");
		league1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tennis01.main(null);
			}
		});
		league1.setBounds(200, 200, 117, 29);
		league1.resize(300, 50);
		contentPane.add(league1);
		
		JButton league2 = new JButton("Tennis Championship League 02");
		league2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tennis02.main(null);
			}
		});
		league2.setBounds(200, 250, 117, 29);
		league2.resize(300, 50);
		contentPane.add(league2);
		
		JButton league3 = new JButton("Tennis Championship League 03");
		league3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tennis03.main(null);
			}
		});
		league3.setBounds(200, 300, 117, 29);
		league3.resize(300, 50);
		contentPane.add(league3);
		
		JButton league4 = new JButton("Tennis Championship League 04");
		league4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tennis04.main(null);
			}
		});
		league4.setBounds(200, 350, 117, 29);
		league4.resize(300, 50);
		contentPane.add(league4);
		
		JButton league5 = new JButton("Tennis Championship League 05");
		league5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tennis05.main(null);
			}
		});
		league5.setBounds(200, 400, 117, 29);
		league5.resize(300, 50);
		contentPane.add(league5);
		
		JButton exit = new JButton("Close");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit.setBounds(200, 450, 117, 29);
		exit.resize(300, 50);
		contentPane.add(exit);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TennisGUI frame = new TennisGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});}

}
