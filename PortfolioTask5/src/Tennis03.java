import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class Tennis03{
  public static void main(String[] args) {
    List<TennisClub> table = Arrays.asList(
    		new TennisClub(1, "Tumbler Terries", 7, 5, 2, 200, 500),
            new TennisClub(2, "Racket Rabbits", 10, 4, 6, 622, 475),
            new TennisClub(3, "Match Points", 2, 0, 2, 0, 241),
            new TennisClub(4, "Survivors", 0, 0, 0, 0, 0),
            new TennisClub(5, "The Heatstroke�s", 4, 4, 0, 0, 400),
            new TennisClub(6, "Mighty Mingles", 2, 1, 1, 80, 100),
            new TennisClub(7, "Tennis the Menace",9, 9, 0, 0, 941),
            new TennisClub(8, "Queens of the Court",8, 3, 5, 569,320),
            new TennisClub(9, "Tennis Junkies",6, 5, 1, 75, 622),
            new TennisClub(10, "Casual Sets",3, 3, 0, 0,393),
            new TennisClub(11, "Double Shots",0, 0, 0, 0, 0),
            new TennisClub(12, "Alley Gators", 7, 4, 3,82,420));

    OptionalInt min = table.stream().mapToInt(TennisClub::getPoints).min();
    OptionalInt max = table.stream().mapToInt(TennisClub::getPoints).max();
    Integer reduce = table.stream().map(TennisClub::getLost).reduce(0, (i, j) -> i + j);
    
    if (min.isPresent()) {
      System.out.printf("\nMinimum Score :  %d\n", min.getAsInt());
    } else {
    	System.out.println("Attempt failed!");
    }
    if(max.isPresent()) {
    	System.out.printf("\nMaximum Score :  %d\n", max.getAsInt());
    }else {
    	System.out.println("Attempt failed!");
    }
    System.out.printf("\nLost Match Count : %d\n",reduce);
    List<String> str = table.stream()
    	    .filter(p -> p.getLost() < 6)
    	    .map(TennisClub::getClub)
    	    .collect(Collectors.toList());  	                   
    	System.out.println("\nClubs who won more than 6 Matches : \n" + str.toString()); 
  
  try {
		FileWriter writer = new FileWriter("Tennis03.txt");
		writer.write("    Club 		    Score   Matches Won  Matches Lost");
		writer.write("  _________                _______  ___________  _____________\n");
		table.forEach(x -> {
		try {
			writer.write(x + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	});
		writer.close();
		System.out.println("\nTennis03.txt - Written Successfully!");
} catch (IOException e) {
	System.out.println("Attempt Failed!");
	e.printStackTrace();
}
}}
