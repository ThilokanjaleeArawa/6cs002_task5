import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Tennis01 {
  public static void main(String[] args) {
    List<TennisClub> table = Arrays.asList(	
        new TennisClub(1, "Tumbler Terries", 7, 5, 2, 200, 500),
        new TennisClub(2, "Racket Rabbits", 10, 4, 6, 622, 475),
        new TennisClub(3, "Match Points", 2, 0, 2, 0, 241),
        new TennisClub(4, "Survivors", 0, 0, 0, 0, 0),
        new TennisClub(5, "The Heatstroke�s", 4, 4, 0, 0, 400),
        new TennisClub(6, "Mighty Mingles", 2, 1, 1, 80, 100),
        new TennisClub(7, "Tennis the Menace",9, 9, 0, 0, 941),
        new TennisClub(8, "Queens of the Court",8, 3, 5, 569,320),
        new TennisClub(9, "Tennis Junkies",6, 5, 1, 75, 622),
        new TennisClub(10, "Casual Sets",3, 3, 0, 0,393),
        new TennisClub(11, "Double Shots",0, 0, 0, 0, 0),
        new TennisClub(12, "Alley Gators", 7, 4, 3,82,420));
    
    System.out.println("    Club 		    Score   Matches Won  Matches Lost");
	System.out.println("  _________                _______  ___________  _____________\n");
     
	table.forEach(x -> System.out.println(x)); 
  try {
		FileWriter writer = new FileWriter("Tennis01.txt");
		writer.write("    Club 		    Score   Matches Won  Matches Lost");
		writer.write("  _________                _______  ___________  _____________\n");
		table.forEach(x -> {
		try {
			writer.write(x + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	});
		writer.close();
		System.out.println("\nTennis01.txt - Written Successfully!");
  } catch (IOException e) {
  	System.out.println("Attempt failed!");
  	e.printStackTrace();
  }
}
}



