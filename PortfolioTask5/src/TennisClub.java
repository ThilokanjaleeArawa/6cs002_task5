public class TennisClub implements Comparable<TennisClub> {
  private int position;
  private String club;
  private int match;
  private int won;
  private int lost;
  private int pointsDifference;
  private int points;

  public TennisClub(int position, String club, int match, int won,int lost, int pointsDifference,int points) {
    this.position = position;
    this.club = club;
    this.match = match;
    this.won = won;
    this.lost = lost;
    this.pointsDifference = pointsDifference;
    this.points = points;
  }
  public String toString() {
    return String.format("%-3d%-20s%10d%10d%10d", position, club, points, won, lost);
  }
  public int getPosition() {
    return position;
  }
  public void setPosition(int position) {
    this.position = position;
  }
  public String getClub() {
    return club;
  }
  public void setClub(String club) {
    this.club = club;
  }
  public int getMatch() {
    return match;
  }
  public void setMatch(int match) {
    this.match = match;
  }
  public int getWon() {
    return won;
  }
  public void setWon(int won) {
    this.won = won;
  }
  public int getLost() {
    return lost;
  }
  public void setLost(int lost) {
    this.lost = lost;
  }
  public int getPointsDifference() {
    return pointsDifference;
  }
  public void setPointsDifference(int pointsDifference) {
    this.pointsDifference = pointsDifference;
  }
  public int getPoints() {
    return points;
  }
  public void setPoints(int points) {
    this.points = points;
  }
@Override
public int compareTo(TennisClub o) {
	// TODO Auto-generated method stub
	return 0;
}
  }
