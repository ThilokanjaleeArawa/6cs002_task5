package Files;

import java.io.*;

public class File01 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("src/wolf-fox.txt"));

    r.lines().forEach(l -> System.out.println(l));

    r.close();
  }

}
